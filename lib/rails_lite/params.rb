require 'uri'

class Params
  # use your initialize to merge params from
  # 1. query string
  # 2. post body
  # 3. route params
  def initialize(req, route_params = {})
    @req = req
    if req.body.nil?
      @params = parse_www_encoded_form(@req.query_string)
    else
      @params = parse_www_encoded_form(@req.body)
    end
  end

  def [](key)
    @params[key]
  end

  def to_s
  end

  private
  # this should return deeply nested hash
  # argument format
  # user[address][street]=main&user[address][zip]=89436
  # should return
  # { "user" => { "address" => { "street" => "main", "zip" => "89436" } } }
  def parse_www_encoded_form(www_encoded_form)
    params = {}

    decoded = URI.decode_www_form(www_encoded_form)
    decoded.each do |arr|
      keys = parse_key(arr[0])
      if keys.count > 1
          keys.reverse.each do |key|
           params = { key => arr[1] }
           arr[1] = params
          end
      else
        params[arr[0]] = arr[1]
      end
    end

    params
  end

  # this should return an array
  # user[address][street] should return ['user', 'address', 'street']
  def parse_key(key)
    key.to_s.gsub(']','').split('[')
  end
end